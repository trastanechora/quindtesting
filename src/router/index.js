import Vue from 'vue';
import VueRouter from 'vue-router';

import User from '../components/User';
import Footer from '../components/Footer';
import ChangePassword from '../components/ChangePassword';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: User
    },
    {
        path: '/user',
        component: User
    },
    {
        path: '/footer',
        components: Footer
    },
    {
        path: '/changepassword',
        components: ChangePassword,
        name: 'ChangePassword'
    }
];

const router = new VueRouter( {
    routes
})

export default router;